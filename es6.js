// topics to covered
// Object shorthand
// Object destruction
// Spread operator
// rest operator
// default argument
// template literals
// arrow notation function
// import and export
// block scope
// class
// ?

// object shorthand

var addr = 'broadway';

var obj = {
  abc: 'uz',
  y: 'z',
  addr
}
console.log('obj is >>', obj)
var email = 'jskisor';

// sendEmail({email}) //shorthand

// object destruction
var fruits = 'grape'

function getSomething() {
  return {
    fish: 'gold fish',
    fruits: ['apple'],
    vegitables: 'potato'
  }
}

// var result = getSomething();

// console.log('result >>', result)

// object desctruction
var { fruits, fish: random } = getSomething();
console.log('fruits is >>', fruits)
console.log('fish is >>', random)


// spread operator
// (...)
// 

var mouse = {
  brand: 'logitech',
  price: 222,
  color: 'white'
}

var mouse1 = {
  origin: 'nepal',
  generation: '2'
}

var mouse2 = {
  ...mouse,
  ...mouse1,
  new: 'newVal'
};

mouse.color = 'red'

console.log('mouse >', mouse);
console.log('mouse 2 >>', mouse2)

// rest operator

// destruction

var { generation: GEN, ...rest } = mouse2;
console.log('GEN >>', GEN)
console.log('rest is >>', rest)

// arrow notation function


// default argument

// function welcome(name = 'broadway') {
//   console.log('name is >>', name)
// }

// welcome()

// string concatination

// template literals

// var text 'lsdjkfj '+ val2 + lkdsjf+'va2'
var nepal = 'nepal'

var text = `welcome to

braodway
infoyss ${nepal}`

console.log('text is >>', text)


// arrow notation function

// const welcome = function(name){
//   // return (return block)
// }

// // one liner function
// const welcome = name => (return block)

// const welcome = name => {
//   return `hi ${name}`
// }

// const welcome = name => `hi ${name}`
// console.log('welcome >>', welcome('braodway'))

var laptops = [{
  name: 'xps',
  generation: 'i5'
}, {
  name: 'legion',
  generation: 'i7'
},
{
  name: 'nitro',
  generation: 'i7'
}]

const i7Laptops = laptops.filter(function (item) {
  if (item.generation === 'i7') {
    return true;
  }
})
console.log('i 7 laptops >>', i7Laptops)

const i7Arrow = laptops.filter(item => item.generation === 'i7');

console.log('same result in arrow notation>', i7Arrow)

// advantages
// arrow notation function will inherit parent's this


// import and export
// export
// we can export in two ways
// named export 
// default export


// named export 
// syntax export const data = <any_value>
// eg export class TEST {}
// there can be multiple named export within file

// default export
// syntax export default <any_value>

// there can be only one default export

// one file can have multiple named export and single default export


// import
// import totatly depends on how it is exported

// if named export

// syntax
import { name1, name2, name3 } from '.path_to_source_file';


// if default export
// import anyName from 'path to_source file'

// if bothe named and default export

// import {name,name2},anyName from 'source file'


// let keyword for block scope

// class == > class is group of contructor methods and fields

class Fruits {
  color;
  constructor() {
    this.color = 'yellow';
  }

  getColor() {
    return this.color;
  }


  setColor(newColor) {
    this.color = newColor;
  }


}

var apple = new Fruits();
// apple.

// inheritance


class Vegitables extends Fruits {
  origin;
  constructor() {
    super(); //parent class ko constructor call
    // before accessing this
    this.origin = 'nepal'
  }

  getOrigin() {
    return this.origin;
  }
}

// var potato = new Vegitables();
// potato.

// react 

// JSX ==> javascript extended syntax
// 