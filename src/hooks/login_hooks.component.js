import React, { useState, useEffect } from 'react';
// import { Link } from 'react-router-dom';
import { SubmitButton } from '../components/Common/SubmitButton/SubmitButton.component';
export const Login = () => {
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [rememeber_me, setRemeberMe] = useState(200);
  const [formData, setFormData] = useState({
    username: '',
    password: ''
  })



  // useState will take initial value

  const handleSubmit = (e) => {
    e.preventDefault();
    setIsSubmitting(true)


  }

  const handleChange = (e) => {
    let { name, value, type, checked } = e.target;
    if (type === 'checkbox') {
      return setRemeberMe(500)
    }
    setFormData({
      ...formData,
      [name]: value
    })
  }

  useEffect(() => {
    console.log('once component is loaded')
    console.log('isSubmittin g ', isSubmitting)
    console.log('remember me g ', rememeber_me)
  }, [isSubmitting])

  // effect hooks
  // syntax
  // useEffect(()=>{
// used as componentDidMount
  // })

  // useEffect(() => {
// used as componentDidUpdate
  // }, [rememeber_me, isSubmitting])

  // useEffect(()=>{
  //   return ()=>{
    // componentWillUnMount();
  // clean up (componenet destroy huda memory leakage prevent garne code)
  //   }
  // },[args])

  // effects can be with cleanup and without cleanup


  return (
    <div className="auth_box">
      <h2>Login</h2>
      <p>Please login to start your session!</p>
      <p>Remember me :{rememeber_me}</p>
      <form className="form-group" onSubmit={handleSubmit}>
        <label htmlFor="username">Username</label>
        <input className="form-control" type="text" id="username" name="username" placeholder="Username" onChange={handleChange}></input>
        {/* <p className="error">{this.state.error.username}</p> */}
        <label htmlFor="password">Password</label>
        <input className="form-control" type="password" id="password" name="password" placeholder="Password" onChange={handleChange}></input>
        {/* <p className="error">{this.state.error.password}</p> */}

        <input type="checkbox" name="rememberMe" onChange={handleChange}></input>
        <label> &nbsp;Remeber me</label>
        <hr />
        <SubmitButton
          isSubmitting={isSubmitting}
          enabledLabel="Login"
          disabledLabel="Loging in..."
        >

        </SubmitButton>
      </form>
      <p>Don't Have an Account?</p>
      {/* <p style={{ float: 'left' }}>Register <Link to="/register">here</Link></p> */}
      {/* <p style={{ float: 'right' }}><Link to="/forgot_password">forgot password?</Link></p> */}
    </div>
  )
}