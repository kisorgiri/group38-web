// Component Life Cycle

// react 

// init ==> constructor , render, ComponentDidMount()
// update ==> render, ComponentDidUpdate()
// destroy ==> componentWillUnMount();

// why redux?
// to manage centralize state of application

// FLux architecture

// basic web architecture
// MVC ==>  models, views, controller
// models <==> controller bidirectional data flow

// FLUX architecture
// concerns ==> Views, Actions, Dispatcher, Store
// unidirectional data flow 
// Views ===> Actions ===> Dispatcher ==> Store
// there can be multiple store in flux

// REDUX 
// concerns ==> views, actions, reducers, store
// single source of truth ==> single store
// unidirectional data flow 
// views ===> Actions ===> reducers ==> store 

// once reducer setup is complete
// store is done and it is connected to react app
// main file ma provider le wrap garne
// component ma connect le wrap garne

// new action

// action ma define
// reducer ma logic build garne
// and then call action through component
// take update data from store from component

// hooks
// --> available from react 16.8

// hooks is a function which is used to enhance functional component

// we can maintain state inside functional component
// effects can be implemented inside functional component


// basic hooks
// ---> state hooks useState();
// ---> effect hooks useEffect();
// ----> context hooks ==> useContext();

// advanced hooks
// useReducer
// useCallback
// useMemo
// useRef
// useImperativeHandle
// useLayoutEffect
// useDebugValue


// use can be identified with use prefix in any function name


// rules of using hooks

// hooks cannot be called from class  based component
// hooks cannot be invoked inside loop, nested functions
// hooks cannot be called from regular JS function we need to create custom hooks to access inbuilt hooks


// migrating from v5 to v6
// Switch is replace by Routes ===> switch was optional but Routes is mandatory

// route ==> component is removed and elementis added
// componenet = {name of component}
// element =={<Componen t></Component>}
// it is easier to supply props

// history match and location is not available in v6
// we need to use custom hooks supplied from 'react router dom '

// useNavigate(); for navigation similar to history
// useParams ==> similar to match
// useLocation ==> similar to location

// for page not found
{/* <Route path="*" element= {<notFound></notFound>}></Route> */}